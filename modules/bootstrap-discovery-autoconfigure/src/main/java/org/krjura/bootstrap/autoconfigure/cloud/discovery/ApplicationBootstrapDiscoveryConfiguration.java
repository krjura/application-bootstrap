package org.krjura.bootstrap.autoconfigure.cloud.discovery;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties( prefix = "org.krjura.bootstrap.discovery")
public class ApplicationBootstrapDiscoveryConfiguration {

    private String name;

    private List<String> tags;

    private String address;

    private Integer port;

    private boolean enableTagOverride;

    private ApplicationBootstrapDiscoveryCheckConfiguration check;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public boolean isEnableTagOverride() {
        return enableTagOverride;
    }

    public void setEnableTagOverride(boolean enableTagOverride) {
        this.enableTagOverride = enableTagOverride;
    }

    public ApplicationBootstrapDiscoveryCheckConfiguration getCheck() {
        return check;
    }

    public void setCheck(ApplicationBootstrapDiscoveryCheckConfiguration check) {
        this.check = check;
    }
}
