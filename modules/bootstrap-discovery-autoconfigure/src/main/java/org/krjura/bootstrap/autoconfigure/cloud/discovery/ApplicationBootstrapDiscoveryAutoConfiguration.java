package org.krjura.bootstrap.autoconfigure.cloud.discovery;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "org.krjura.bootstrap.discovery.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({DiscoveryClient.class})
public class ApplicationBootstrapDiscoveryAutoConfiguration {

    @Bean
    public ApplicationBootstrapDiscoveryConfiguration applicationBootstrapDiscoveryConfiguration() {
        return new ApplicationBootstrapDiscoveryConfiguration();
    }

    @Bean
    public ApplicationBootstrapDiscoveryBaseConfiguration applicationBootstrapDiscoveryBaseConfiguration() {
        return new ApplicationBootstrapDiscoveryBaseConfiguration();
    }

    @Bean
    public ApplicationBootstrapDiscoveryRegistration applicationBootstrapDiscoveryRegistration(
            ApplicationBootstrapDiscoveryConfiguration configuration,
            ApplicationBootstrapDiscoveryBaseConfiguration baseConfiguration) {

        return new ApplicationBootstrapDiscoveryRegistration(baseConfiguration, configuration);
    }
}
