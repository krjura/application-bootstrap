package org.krjura.bootstrap.autoconfigure.cloud.discovery;

import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.types.consul.ConsulApiClient;
import org.krjura.bootstrap.client.types.consul.pojo.response.catalog.ConsulCatalogNodesForServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ApplicationBootstrapDiscoveryClient implements DiscoveryClient {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationBootstrapDiscoveryClient.class);

    private static final ServiceInstanceMapper SERVICE_INSTANCE_MAPPER = new ServiceInstanceMapper();

    private ConsulApiClient consulApiClient;

    public ApplicationBootstrapDiscoveryClient(ConsulApiClient client) {
        this.consulApiClient = client;
    }

    @Override
    public String description() {
        return "ApplicationBootstrapDiscoveryClient";
    }

    @Override
    @Deprecated
    public ServiceInstance getLocalServiceInstance() {
        throw new UnsupportedOperationException("method not supported");
    }

    @Override
    public List<ServiceInstance> getInstances(String serviceId) {
        try {
            return consulApiClient
                    .getConsulCatalogClient()
                    .listCatalogNodesForService(serviceId)
                    .stream()
                    .map(SERVICE_INSTANCE_MAPPER)
                    .collect(Collectors.toList());
        } catch (BootstrapException e) {
            logger.warn("cannot fetch agent services", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<String> getServices() {
        throw new UnsupportedOperationException("method not supported");
    }

    private static class ServiceInstanceMapper
            implements Function<ConsulCatalogNodesForServiceResponse, ServiceInstance> {

        @Override
        public ServiceInstance apply(ConsulCatalogNodesForServiceResponse service) {
            return new DefaultServiceInstance(
                    service.getServiceId(),
                    service.getAddress(), service.getServicePort(),
                    false, service.getNodeMeta());
        }
    }
}
