package org.krjura.bootstrap.autoconfigure.cloud.discovery;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( prefix = "org.krjura.bootstrap.discovery.check")
public class ApplicationBootstrapDiscoveryCheckConfiguration {

    private String name = "Health check";

    private String interval = "10s";

    private String notes = "N/A";

    private String deregisterCriticalServiceAfter = "10m";

    private String http;

    private String method = "GET";

    private boolean tlsSkipVerify = false;

    private String tcp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDeregisterCriticalServiceAfter() {
        return deregisterCriticalServiceAfter;
    }

    public void setDeregisterCriticalServiceAfter(String deregisterCriticalServiceAfter) {
        this.deregisterCriticalServiceAfter = deregisterCriticalServiceAfter;
    }

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isTlsSkipVerify() {
        return tlsSkipVerify;
    }

    public void setTlsSkipVerify(boolean tlsSkipVerify) {
        this.tlsSkipVerify = tlsSkipVerify;
    }

    public String getTcp() {
        return tcp;
    }

    public void setTcp(String tcp) {
        this.tcp = tcp;
    }

    @Override
    public String toString() {
        return "ApplicationBootstrapDiscoveryCheckConfiguration{" +
                "name='" + name + '\'' +
                ", interval='" + interval + '\'' +
                ", notes='" + notes + '\'' +
                ", deregisterCriticalServiceAfter='" + deregisterCriticalServiceAfter + '\'' +
                ", http='" + http + '\'' +
                ", method='" + method + '\'' +
                ", tlsSkipVerify=" + tlsSkipVerify +
                ", tcp='" + tcp + '\'' +
                '}';
    }
}
