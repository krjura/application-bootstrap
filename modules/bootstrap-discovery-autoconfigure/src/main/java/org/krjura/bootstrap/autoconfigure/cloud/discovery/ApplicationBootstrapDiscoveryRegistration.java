package org.krjura.bootstrap.autoconfigure.cloud.discovery;

import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.types.consul.ConsulApiClient;
import org.krjura.bootstrap.client.types.consul.pojo.request.ConsulCheckRequest;
import org.krjura.bootstrap.client.types.consul.pojo.request.ConsulRegisterServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.UUID;

@Component
public class ApplicationBootstrapDiscoveryRegistration {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationBootstrapDiscoveryRegistration.class);

    private final ApplicationBootstrapDiscoveryBaseConfiguration baseConfiguration;

    private final ApplicationBootstrapDiscoveryConfiguration configuration;

    private ConsulApiClient consulApiClient;

    private String checkUuid;

    private String serviceUuid;

    public ApplicationBootstrapDiscoveryRegistration(
            ApplicationBootstrapDiscoveryBaseConfiguration baseConfiguration,
            ApplicationBootstrapDiscoveryConfiguration configuration) {

        Assert.notNull(configuration, "ApplicationBootstrapDiscoveryConfiguration cannot be null");
        Assert.notNull(baseConfiguration, "ApplicationBootstrapDiscoveryBaseConfiguration cannot be null");

        this.baseConfiguration = baseConfiguration;
        this.configuration = configuration;

        createConsulClient();
    }

    private void createConsulClient() {
        ApplicationBootstrapBaseConfiguration consulConfiguration = ApplicationBootstrapBaseConfiguration.Builder
                .instance()
                .setBaseUrl(this.baseConfiguration.getBaseUrl())
                .setToken(this.baseConfiguration.getToken())
                .isEnabled(this.baseConfiguration.isEnabled())
                .isRetryWhenFailed(this.baseConfiguration.isRetryWhenFailed())
                .setNumberOfRetries(this.baseConfiguration.getNumberOfRetries())
                .setPauseBetweenRetries(this.baseConfiguration.getPauseBetweenRetries())
                .build();

        this.consulApiClient = new ConsulApiClient(consulConfiguration);
    }

    @PostConstruct
    public void init() {
        logger.info("registering service");

        String checkUuid = UUID.randomUUID().toString();
        String serviceUuid = UUID.randomUUID().toString();

        ConsulCheckRequest checkRequest = new ConsulCheckRequest();
        checkRequest.setName(this.configuration.getCheck().getName());
        checkRequest.setId(checkUuid);
        checkRequest.setInterval(this.configuration.getCheck().getInterval());
        checkRequest.setNotes(this.configuration.getCheck().getNotes());
        checkRequest.setDeregisterCriticalServiceAfter(this.configuration.getCheck().getDeregisterCriticalServiceAfter());
        checkRequest.setHttp(this.configuration.getCheck().getHttp());
        checkRequest.setMethod(this.configuration.getCheck().getMethod());
        checkRequest.setTcp(this.configuration.getCheck().getTcp());
        checkRequest.setTlsSkipVerify(this.configuration.getCheck().isTlsSkipVerify());

        ConsulRegisterServiceRequest request = new ConsulRegisterServiceRequest();
        request.setName(this.configuration.getName());
        request.setId(serviceUuid);
        request.setAddress(this.configuration.getAddress());
        request.setPort(this.configuration.getPort());
        request.setEnableTagOverride(this.configuration.isEnableTagOverride());
        request.setTags(this.configuration.getTags());
        request.setCheck(checkRequest);

        try {
            consulApiClient.getConsulAgentClient().registerAgentService(request);

            this.checkUuid = checkUuid;
            this.serviceUuid = serviceUuid;

            logger.info("registration complete");
        } catch (BootstrapException e) {
            logger.warn("Cannot register service", e);
        }
    }

    @PreDestroy
    public void destroy() {
        try {
            logger.info("un-registering service");
            this.consulApiClient.getConsulAgentClient().unRegisterAgentService(this.serviceUuid);
            logger.info("un-registration complete");
        } catch (BootstrapException e) {
            logger.warn("Cannot un-register service with uuid " + serviceUuid, e);
        }
    }
}
