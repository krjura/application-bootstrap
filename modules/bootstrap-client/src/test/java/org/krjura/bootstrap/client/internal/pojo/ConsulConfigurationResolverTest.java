package org.krjura.bootstrap.client.internal.pojo;

import org.junit.After;
import org.junit.Test;
import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.pojo.ExternalFileMapping;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.anyOf;

public class ConsulConfigurationResolverTest {

    @After
    public void after() {
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_BASE_URL);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_TOKEN);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_ENABLED);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_RETRY_WHEN_FAILED);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_NUMBER_OF_RETRIES);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_PAUSE_BETWEEN_RETRIES);
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_CONFIG_FILE);
    }

    @Test
    public void testWhenNothingIsSet() throws BootstrapException {
        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();

        ApplicationBootstrapBaseConfiguration configuration = resolver.build();
        ExternalFileMappings mappings = resolver.buildMappings();

        assertThat(configuration.getBaseUrl(), is(equalTo("http://localhost:8500")));
        assertThat(configuration.isEnabled(), is(equalTo(false)));
        assertThat(configuration.isRetryWhenFailed(), is(equalTo(false)));
        assertThat(configuration.getNumberOfRetries(), is(equalTo(0)));
        assertThat(configuration.getPauseBetweenRetries(), is(equalTo(5000)));
        assertThat(configuration.getToken(), is(nullValue()));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings().size(), is(equalTo(0)));
    }

    @Test
    public void testWhenSystemPropertiesAreSet() throws BootstrapException {
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_BASE_URL, "http://127.0.0.1");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_TOKEN, "token");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_ENABLED, "true");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_RETRY_WHEN_FAILED, "true");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_NUMBER_OF_RETRIES, "10");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_PAUSE_BETWEEN_RETRIES, "10000");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_MAPPINGS + ".1", "tmp->tmp2");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_MAPPINGS + ".2", "tmp3->tmp4");

        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();

        ApplicationBootstrapBaseConfiguration configuration = resolver.build();
        ExternalFileMappings mappings = resolver.buildMappings();

        assertThat(configuration.getBaseUrl(), is(equalTo("http://127.0.0.1")));
        assertThat(configuration.isEnabled(), is(equalTo(true)));
        assertThat(configuration.isRetryWhenFailed(), is(equalTo(true)));
        assertThat(configuration.getNumberOfRetries(), is(equalTo(10)));
        assertThat(configuration.getPauseBetweenRetries(), is(equalTo(10000)));
        assertThat(configuration.getToken(), is(equalTo("token")));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings().size(), is(equalTo(2)));

        // cannot be sure which order since it is a set
        ExternalFileMapping mappingZero = mappings.getMappings().get(0);
        assertThat(mappingZero.getPath(), anyOf(equalTo("tmp"), equalTo("tmp3")));
        assertThat(mappingZero.getFilename(), anyOf(equalTo("tmp2"), equalTo("tmp4")));

        ExternalFileMapping mappingOne = mappings.getMappings().get(1);
        assertThat(mappingOne.getPath(), anyOf(equalTo("tmp"), equalTo("tmp3")));
        assertThat(mappingOne.getFilename(), anyOf(equalTo("tmp2"), equalTo("tmp4")));

        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_MAPPINGS + ".1");
        System.clearProperty(ApplicationBootstrapConfigurationResolver.PROP_MAPPINGS + ".2");
    }

    @Test
    public void testWhenConfigFileIsSet() throws BootstrapException {
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_CONFIG_FILE, "etc/test-config.properties");

        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();

        ApplicationBootstrapBaseConfiguration configuration = resolver.build();
        ExternalFileMappings mappings = resolver.buildMappings();

        assertThat(configuration.getBaseUrl(), is(equalTo("http://example.com")));
        assertThat(configuration.isEnabled(), is(equalTo(true)));
        assertThat(configuration.isRetryWhenFailed(), is(equalTo(true)));
        assertThat(configuration.getNumberOfRetries(), is(equalTo(50)));
        assertThat(configuration.getPauseBetweenRetries(), is(equalTo(50000)));
        assertThat(configuration.getToken(), is(equalTo("test")));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings().size(), is(equalTo(2)));

        // cannot be sure which order since it is a set
        ExternalFileMapping mappingZero = mappings.getMappings().get(0);
        assertThat(mappingZero.getPath(), anyOf(equalTo("test"), equalTo("test3")));
        assertThat(mappingZero.getFilename(), anyOf(equalTo("test2"), equalTo("test4")));

        ExternalFileMapping mappingOne = mappings.getMappings().get(1);
        assertThat(mappingOne.getPath(), anyOf(equalTo("test"), equalTo("test3")));
        assertThat(mappingOne.getFilename(), anyOf(equalTo("test2"), equalTo("test4")));
    }

    @Test
    public void testOverrideConfigFileUsingSystemProperties() throws BootstrapException {
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_CONFIG_FILE, "etc/test-config.properties");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_NUMBER_OF_RETRIES, "10");
        System.setProperty(ApplicationBootstrapConfigurationResolver.PROP_PAUSE_BETWEEN_RETRIES, "10000");

        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();

        ApplicationBootstrapBaseConfiguration configuration = resolver.build();
        ExternalFileMappings mappings = resolver.buildMappings();

        assertThat(configuration.getBaseUrl(), is(equalTo("http://example.com")));
        assertThat(configuration.isEnabled(), is(equalTo(true)));
        assertThat(configuration.isRetryWhenFailed(), is(equalTo(true)));
        assertThat(configuration.getNumberOfRetries(), is(equalTo(10)));
        assertThat(configuration.getPauseBetweenRetries(), is(equalTo(10000)));
        assertThat(configuration.getToken(), is(equalTo("test")));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings(), is(notNullValue()));
        assertThat(mappings.getMappings().size(), is(equalTo(2)));

        // cannot be sure which order since it is a set
        ExternalFileMapping mappingZero = mappings.getMappings().get(0);
        assertThat(mappingZero.getPath(), anyOf(equalTo("test"), equalTo("test3")));
        assertThat(mappingZero.getFilename(), anyOf(equalTo("test2"), equalTo("test4")));

        ExternalFileMapping mappingOne = mappings.getMappings().get(1);
        assertThat(mappingOne.getPath(), anyOf(equalTo("test"), equalTo("test3")));
        assertThat(mappingOne.getFilename(), anyOf(equalTo("test2"), equalTo("test4")));
    }
}
