package org.krjura.bootstrap.client.internal.pojo;

public class ApplicationBootstrapBaseConfiguration {

    private String baseUrl;

    private String token;

    private boolean enabled;

    private boolean retryWhenFailed;

    private Integer numberOfRetries;

    private Integer pauseBetweenRetries;

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getToken() {
        return token;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isRetryWhenFailed() {
        return retryWhenFailed;
    }

    public Integer getNumberOfRetries() {
        return numberOfRetries;
    }

    public Integer getPauseBetweenRetries() {
        return pauseBetweenRetries;
    }

    public static class Builder {

        private ApplicationBootstrapBaseConfiguration bootstrapConfiguration;

        private Builder() {
            this.bootstrapConfiguration = new ApplicationBootstrapBaseConfiguration();
        }

        public static Builder instance() {
            return new Builder();
        }

        public Builder setBaseUrl(String baseUrl) {
            this.bootstrapConfiguration.baseUrl = baseUrl;
            return this;
        }

        public Builder setToken(String token) {
            this.bootstrapConfiguration.token = token;
            return this;
        }

        public Builder isEnabled(boolean enabled) {
            this.bootstrapConfiguration.enabled = enabled;
            return this;
        }

        public Builder isRetryWhenFailed(boolean retryWhenFailed) {
            this.bootstrapConfiguration.retryWhenFailed = retryWhenFailed;
            return this;
        }

        public Builder setNumberOfRetries(Integer numberOfRetries) {
            this.bootstrapConfiguration.numberOfRetries = numberOfRetries;
            return this;
        }

        public Builder setPauseBetweenRetries(Integer pauseBetweenRetries) {
            this.bootstrapConfiguration.pauseBetweenRetries = pauseBetweenRetries;
            return this;
        }

        public ApplicationBootstrapBaseConfiguration build() {
            return this.bootstrapConfiguration;
        }
    }

}
