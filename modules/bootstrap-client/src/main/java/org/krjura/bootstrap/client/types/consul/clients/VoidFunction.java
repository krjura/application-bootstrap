package org.krjura.bootstrap.client.types.consul.clients;

import org.krjura.bootstrap.client.ex.BootstrapException;

public interface VoidFunction<R> {

    R apply() throws BootstrapException;
}