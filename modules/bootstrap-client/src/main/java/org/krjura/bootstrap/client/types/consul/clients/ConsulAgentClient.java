package org.krjura.bootstrap.client.types.consul.clients;

import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.types.consul.pojo.request.ConsulRegisterServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConsulAgentClient extends ConsulClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulAgentClient.class);

    public ConsulAgentClient(ApplicationBootstrapBaseConfiguration baseConfiguration) {
        super(baseConfiguration);
    }

    public Void registerAgentService(ConsulRegisterServiceRequest request) throws BootstrapException {
        return retry(() -> registerAgentServiceWithRetry(request));
    }

    private Void registerAgentServiceWithRetry(ConsulRegisterServiceRequest request) throws BootstrapException {
        logger.info("registering service");

        String fileURL = getBaseConfiguration().getBaseUrl() + "/v1/agent/service/register";

        HttpURLConnection connection = null;

        try {
            URL url = new URL(fileURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            if(getBaseConfiguration().getToken() != null) {
                connection.setRequestProperty(HTTP_HEADER_CONSUL_TOKEN, getBaseConfiguration().getToken());
            }

            OutputStream outputStream = connection.getOutputStream();
            getObjectMapper().writeValue(outputStream, request);

            checkResponseCode(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", fileURL);
            throw new BootstrapException("Cannot connect to url " + fileURL, e);
        } finally {
            close(connection);
        }

        return null;
    }

    public Void unRegisterAgentService(String serviceId) throws BootstrapException {
        return retry(() -> unRegisterAgentServiceWithRetry(serviceId));
    }

    private Void unRegisterAgentServiceWithRetry(String serviceId) throws BootstrapException {
        logger.info("un-registering service with service id of {}", serviceId);

        String fileURL = getBaseConfiguration().getBaseUrl() + "/v1/agent/service/deregister/" + serviceId;

        HttpURLConnection connection = null;

        try {
            URL url = new URL(fileURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            if(getBaseConfiguration().getToken() != null) {
                connection.setRequestProperty(HTTP_HEADER_CONSUL_TOKEN, getBaseConfiguration().getToken());
            }

            checkResponseCode(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", fileURL);
            throw new BootstrapException("Cannot connect to url " + fileURL, e);
        } finally {
            close(connection);
        }

        return null;
    }
}
