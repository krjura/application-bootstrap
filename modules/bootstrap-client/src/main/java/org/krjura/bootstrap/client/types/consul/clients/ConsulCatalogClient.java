package org.krjura.bootstrap.client.types.consul.clients;

import com.fasterxml.jackson.core.type.TypeReference;
import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.types.consul.pojo.response.catalog.ConsulCatalogNodesForServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ConsulCatalogClient extends ConsulClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulKvClient.class);

    public ConsulCatalogClient(ApplicationBootstrapBaseConfiguration baseConfiguration) {
        super(baseConfiguration);
    }

    public List<ConsulCatalogNodesForServiceResponse> listCatalogNodesForService(String serviceId)
            throws BootstrapException {

        logger.info("calling listCatalogNodesForService");

        String fileURL = getBaseConfiguration().getBaseUrl() + "/v1/catalog/service/" + serviceId;

        HttpURLConnection connection = null;

        try {
            URL url = new URL(fileURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            if(getBaseConfiguration().getToken() != null) {
                connection.setRequestProperty(HTTP_HEADER_CONSUL_TOKEN, getBaseConfiguration().getToken());
            }

            checkResponseCode(connection);

            TypeReference<List<ConsulCatalogNodesForServiceResponse>> typeRef = new
                    TypeReference<List<ConsulCatalogNodesForServiceResponse>>() {};

            return getObjectMapper().readValue(url, typeRef);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", fileURL);
            throw new BootstrapException("Cannot connect to url " + fileURL, e);
        } finally {
            close(connection);
        }
    }
}
