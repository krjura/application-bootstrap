package org.krjura.bootstrap.client.ex;

public class BootstrapException extends Exception {

    public BootstrapException(String message) {
        super(message);
    }

    public BootstrapException(String message, Throwable cause) {
        super(message, cause);
    }
}
