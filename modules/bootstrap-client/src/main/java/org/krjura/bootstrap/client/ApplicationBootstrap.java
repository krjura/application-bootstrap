package org.krjura.bootstrap.client;

import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapConfigurationResolver;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;
import org.krjura.bootstrap.client.types.consul.ConsulApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationBootstrap {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationBootstrap.class);

    public static void bootstrap() throws BootstrapException {
        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();
        ApplicationBootstrapBaseConfiguration configuration = resolver.build();

        bootstrap(configuration, resolver.buildMappings());
    }

    public static void bootstrap(ExternalFileMappings mappings) throws BootstrapException {
        ApplicationBootstrapConfigurationResolver resolver = new ApplicationBootstrapConfigurationResolver();
        ApplicationBootstrapBaseConfiguration configuration = resolver.build();

        bootstrap(configuration, mappings);
    }

    public static void bootstrap(ApplicationBootstrapBaseConfiguration configuration, ExternalFileMappings mappings) {

        try {
            if(!configuration.isEnabled()) {
                logger.info("bootstrap is disabled.");
                return;
            }

            ConsulApiClient apiClient = new ConsulApiClient(configuration);
            apiClient.getConsulKvClient().downloadAndSave(mappings);
        } catch (BootstrapException e) {
            logger.warn("Cannot bootstrap spring. Continuing without it", e);
        }
    }

    public static void main(String[] args) throws BootstrapException {
        ApplicationBootstrap.bootstrap();
    }
}