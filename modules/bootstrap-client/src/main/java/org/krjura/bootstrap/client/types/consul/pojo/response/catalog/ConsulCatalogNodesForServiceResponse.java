package org.krjura.bootstrap.client.types.consul.pojo.response.catalog;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulCatalogNodesForServiceResponse {

    private String id;

    private String node;

    private String address;

    private String dataCenter;

    private TaggedAddresses taggedAddresses;

    private Map<String, String> nodeMeta;

    private String serviceAddress;

    private Integer createIndex;

    private Integer modifyIndex;

    private boolean serviceEnableTagOverride;

    private String serviceId;

    private String serviceName;

    private Integer servicePort;

    private List<String> tags;

    @JsonCreator
    public ConsulCatalogNodesForServiceResponse(
            @JsonProperty("ID") String id,
            @JsonProperty("Node") String node,
            @JsonProperty("Address") String address,
            @JsonProperty("Datacenter") String dataCenter,
            @JsonProperty("TaggedAddresses") TaggedAddresses taggedAddresses,
            @JsonProperty("NodeMeta") Map<String, String> nodeMeta,
            @JsonProperty("ServiceAddress") String serviceAddress,
            @JsonProperty("CreateIndex") Integer createIndex,
            @JsonProperty("ModifyIndex") Integer modifyIndex,
            @JsonProperty("ServiceEnableTagOverride") boolean serviceEnableTagOverride,
            @JsonProperty("ServiceID") String serviceId,
            @JsonProperty("ServiceName") String serviceName,
            @JsonProperty("ServicePort") Integer servicePort,
            @JsonProperty("ServiceTags") List<String> tags) {


        this.id = id;
        this.node = node;
        this.address = address;
        this.dataCenter = dataCenter;
        this.taggedAddresses = taggedAddresses;
        this.nodeMeta = nodeMeta;
        this.serviceAddress = serviceAddress;
        this.createIndex = createIndex;
        this.modifyIndex = modifyIndex;
        this.serviceEnableTagOverride = serviceEnableTagOverride;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.servicePort = servicePort;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public String getNode() {
        return node;
    }

    public String getAddress() {
        return address;
    }

    public String getDataCenter() {
        return dataCenter;
    }

    public TaggedAddresses getTaggedAddresses() {
        return taggedAddresses;
    }

    public Map<String, String> getNodeMeta() {
        return nodeMeta;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public Integer getCreateIndex() {
        return createIndex;
    }

    public Integer getModifyIndex() {
        return modifyIndex;
    }

    public boolean isServiceEnableTagOverride() {
        return serviceEnableTagOverride;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Integer getServicePort() {
        return servicePort;
    }

    public List<String> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return "ConsulCatalogNodesForServiceResponse{" +
                "id='" + id + '\'' +
                ", node='" + node + '\'' +
                ", address='" + address + '\'' +
                ", dataCenter='" + dataCenter + '\'' +
                ", taggedAddresses=" + taggedAddresses +
                ", nodeMeta=" + nodeMeta +
                ", serviceAddress='" + serviceAddress + '\'' +
                ", createIndex=" + createIndex +
                ", modifyIndex=" + modifyIndex +
                ", serviceEnableTagOverride=" + serviceEnableTagOverride +
                ", serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", servicePort=" + servicePort +
                ", tags=" + tags +
                '}';
    }
}
