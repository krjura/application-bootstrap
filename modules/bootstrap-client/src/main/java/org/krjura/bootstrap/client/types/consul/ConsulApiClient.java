package org.krjura.bootstrap.client.types.consul;

import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.types.consul.clients.ConsulAgentClient;
import org.krjura.bootstrap.client.types.consul.clients.ConsulCatalogClient;
import org.krjura.bootstrap.client.types.consul.clients.ConsulKvClient;

public class ConsulApiClient {

    private ApplicationBootstrapBaseConfiguration baseConfiguration;

    public ConsulApiClient(ApplicationBootstrapBaseConfiguration baseConfiguration) {
        this.baseConfiguration = baseConfiguration;
    }

    public ConsulAgentClient getConsulAgentClient() {
        return new ConsulAgentClient(this.baseConfiguration);
    }

    public ConsulCatalogClient getConsulCatalogClient() {
        return new ConsulCatalogClient(this.baseConfiguration);
    }

    public ConsulKvClient getConsulKvClient() {
        return new ConsulKvClient(this.baseConfiguration);
    }
}
