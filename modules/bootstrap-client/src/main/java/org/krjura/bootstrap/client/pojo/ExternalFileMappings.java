package org.krjura.bootstrap.client.pojo;

import java.util.List;

public class ExternalFileMappings {

    private List<ExternalFileMapping> mappings;

    public ExternalFileMappings(List<ExternalFileMapping> mappings) {
        this.mappings = mappings;
    }

    public List<ExternalFileMapping> getMappings() {
        return mappings;
    }
}
