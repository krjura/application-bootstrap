package org.krjura.bootstrap.client.builders;

import org.krjura.bootstrap.client.pojo.ExternalFileMapping;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;

import java.util.ArrayList;
import java.util.List;

public class ExternalFileMappingBuilder {

    private List<ExternalFileMapping> mappings;

    private ExternalFileMappingBuilder() {
        this.mappings = new ArrayList<>();
    }

    public static ExternalFileMappingBuilder instance() {
        return new ExternalFileMappingBuilder();
    }

    public ExternalFileMappingBuilder addMapping(String consulPath, String filename) {
        this.mappings.add(new ExternalFileMapping(consulPath, filename));

        return this;
    }

    public ExternalFileMappings build() {
        return new ExternalFileMappings(this.mappings);
    }
}
