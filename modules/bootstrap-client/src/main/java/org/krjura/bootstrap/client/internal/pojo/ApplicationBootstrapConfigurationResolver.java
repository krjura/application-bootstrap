package org.krjura.bootstrap.client.internal.pojo;

import org.krjura.bootstrap.client.builders.ExternalFileMappingBuilder;
import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationBootstrapConfigurationResolver {

    static final String PROP_BASE_URL = "org.krjura.bootstrap.base.url";
    static final String PROP_TOKEN = "org.krjura.bootstrap.token";
    static final String PROP_ENABLED = "org.krjura.bootstrap.enabled";
    static final String PROP_RETRY_WHEN_FAILED = "org.krjura.bootstrap.retryWhenFailed";
    static final String PROP_NUMBER_OF_RETRIES = "org.krjura.bootstrap.numberOfRetries";
    static final String PROP_PAUSE_BETWEEN_RETRIES = "org.krjura.bootstrap.pauseBetweenRetries";
    static final String PROP_CONFIG_FILE = "org.krjura.bootstrap.configFile";
    static final String PROP_MAPPINGS = "org.krjura.bootstrap.mappings";
    private static final String PROP_SEPARATOR = "->";


    private static final String DEFAULT_BASE_URL = "http://localhost:8500";
    private static final String DEFAULT_ENABLED = "false";
    private static final String DEFAULT_TOKEN = null;
    private static final String DEFAULT_RETRY_WHEN_FAILED = "false";
    private static final String DEFAULT_NUMBER_OF_RETRIES = "0";
    private static final String DEFAULT_PAUSE_BETWEEN_RETRIES = "5000";
    private static final String DEFAULT_CONFIG_FILE = null;

    private String baseUrl;

    private String token;

    private boolean enabled;

    private boolean retryWhenFailed;

    private Integer numberOfRetries;

    private Integer pauseBetweenRetries;

    private ExternalFileMappingBuilder mappingBuilder;

    public ApplicationBootstrapConfigurationResolver() throws BootstrapException {
        this.mappingBuilder = ExternalFileMappingBuilder.instance();

        setDefaults();
        loadConfigurationFromPropertyFile();
        loadConfigurationFromSystem();
    }

    private void setDefaults() {
        this.baseUrl = DEFAULT_BASE_URL;
        this.token = DEFAULT_TOKEN;
        this.enabled = Boolean.parseBoolean(DEFAULT_ENABLED);
        this.retryWhenFailed = Boolean.parseBoolean(DEFAULT_RETRY_WHEN_FAILED);
        this.numberOfRetries = Integer.parseInt(DEFAULT_NUMBER_OF_RETRIES);
        this.pauseBetweenRetries = Integer.parseInt(DEFAULT_PAUSE_BETWEEN_RETRIES);
    }

    private void loadConfigurationFromPropertyFile() throws BootstrapException {
        String configFile = System.getProperty(PROP_CONFIG_FILE, DEFAULT_CONFIG_FILE);

        if(configFile == null) {
            return;
        }

        try(FileInputStream fis = new FileInputStream(configFile)) {
            Properties properties = new Properties();
            properties.load(fis);

            this.baseUrl = properties.getProperty(PROP_BASE_URL, this.baseUrl);
            this.token = properties.getProperty(PROP_TOKEN, this.token);
            this.enabled = Boolean.parseBoolean(properties.getProperty(PROP_ENABLED, Boolean.toString(this.enabled)));
            this.retryWhenFailed = Boolean
                    .parseBoolean(properties.getProperty(PROP_RETRY_WHEN_FAILED, Boolean.toString(this.retryWhenFailed)));
            this.numberOfRetries = Integer
                    .parseInt(properties.getProperty(PROP_NUMBER_OF_RETRIES, this.numberOfRetries.toString()));
            this.pauseBetweenRetries = Integer
                    .parseInt(properties.getProperty(PROP_PAUSE_BETWEEN_RETRIES, this.pauseBetweenRetries.toString()));

            // now try loading mappings
            loadMappingsFromPropertyFile(properties);
        } catch (IOException e) {
            throw new BootstrapException("Cannot load config from property file " + configFile, e);
        }
    }

    private void loadConfigurationFromSystem() {
        this.baseUrl = System.getProperty(PROP_BASE_URL, this.baseUrl);
        this.enabled = Boolean.parseBoolean(System.getProperty(PROP_ENABLED, Boolean.toString(this.enabled)));
        this.token = System.getProperty(PROP_TOKEN, this.token);
        this.retryWhenFailed = Boolean
                .parseBoolean(System.getProperty(PROP_RETRY_WHEN_FAILED, Boolean.toString(this.retryWhenFailed)));
        this.numberOfRetries = Integer
                .parseInt(System.getProperty(PROP_NUMBER_OF_RETRIES, this.numberOfRetries.toString()));
        this.pauseBetweenRetries = Integer
                .parseInt(System.getProperty(PROP_PAUSE_BETWEEN_RETRIES, this.pauseBetweenRetries.toString()));

        // now try loading mappings
        loadMappingsFromPropertyFile(System.getProperties());
    }

    private void loadMappingsFromPropertyFile(Properties properties) {

        for( Object key : properties.keySet()) {
            String keyString = (String) key;

            if(!keyString.startsWith(PROP_MAPPINGS)) {
                continue;
            }

            String value = properties.getProperty(keyString);

            String[] values = value.split(PROP_SEPARATOR);

            if(values.length !=2 ) {
                continue;
            }

            this.mappingBuilder.addMapping(values[0], values[1]);
        }
    }

    public ApplicationBootstrapBaseConfiguration build() {
        return ApplicationBootstrapBaseConfiguration.Builder
                .instance()
                .setBaseUrl(this.baseUrl)
                .setToken(this.token)
                .isEnabled(this.enabled)
                .isRetryWhenFailed(this.retryWhenFailed)
                .setNumberOfRetries(this.numberOfRetries)
                .setPauseBetweenRetries(this.pauseBetweenRetries)
                .build();
    }

    public ExternalFileMappings buildMappings() {
        return this.mappingBuilder.build();
    }
}
