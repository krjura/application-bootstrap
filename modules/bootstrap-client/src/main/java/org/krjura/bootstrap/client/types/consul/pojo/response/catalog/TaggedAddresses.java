package org.krjura.bootstrap.client.types.consul.pojo.response.catalog;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaggedAddresses {

    private String lan;

    private String wan;

    @JsonCreator
    public TaggedAddresses(
            @JsonProperty("lan") String lan,
            @JsonProperty("wan") String wan) {

        this.lan = lan;
        this.wan = wan;
    }

    public String getLan() {
        return lan;
    }

    public String getWan() {
        return wan;
    }

    @Override
    public String toString() {
        return "TaggedAddresses{" +
                "lan='" + lan + '\'' +
                ", wan='" + wan + '\'' +
                '}';
    }
}
