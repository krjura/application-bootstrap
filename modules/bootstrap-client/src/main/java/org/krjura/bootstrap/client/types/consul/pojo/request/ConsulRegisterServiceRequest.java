package org.krjura.bootstrap.client.types.consul.pojo.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ConsulRegisterServiceRequest {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("ID")
    private String id;

    @JsonProperty("Tags")
    private List<String> tags;

    @JsonProperty("Address")
    private String address;

    @JsonProperty("Port")
    private Integer port;

    @JsonProperty("Check")
    private ConsulCheckRequest check;

    @JsonProperty("EnableTagOverride")
    private boolean enableTagOverride;

    public ConsulRegisterServiceRequest() {
        this.tags = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public ConsulCheckRequest getCheck() {
        return check;
    }

    public void setCheck(ConsulCheckRequest check) {
        this.check = check;
    }

    public boolean isEnableTagOverride() {
        return enableTagOverride;
    }

    public void setEnableTagOverride(boolean enableTagOverride) {
        this.enableTagOverride = enableTagOverride;
    }
}
