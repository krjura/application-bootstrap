package org.krjura.bootstrap.client.types.consul.clients;

import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.types.consul.pojo.custom.ConsulKv;
import org.krjura.bootstrap.client.types.consul.pojo.response.kv.ConsulRawKvResponse;
import org.krjura.bootstrap.client.pojo.ExternalFileMapping;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConsulKvClient extends ConsulClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulKvClient.class);

    public ConsulKvClient(ApplicationBootstrapBaseConfiguration baseConfiguration) {
        super(baseConfiguration);
    }

    public ConsulKv downloadEntry(String consulPath) throws BootstrapException {
        logger.info("downloading file from consul at path {}", consulPath);

        String fileURL = getBaseConfiguration().getBaseUrl() + "/v1/kv" + consulPath;

        HttpURLConnection connection = null;

        try {
            URL url = new URL(fileURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            if(getBaseConfiguration().getToken() != null) {
                connection.setRequestProperty(HTTP_HEADER_CONSUL_TOKEN, getBaseConfiguration().getToken());
            }

            checkResponseCode(connection);

            // opens input stream from the HTTP connection
            InputStream inputStream = connection.getInputStream();

            ConsulRawKvResponse[] kvs = getObjectMapper().readValue(inputStream, ConsulRawKvResponse[].class);

            List<ConsulKv> kvList = Arrays.stream(kvs).map(ConsulKv::new).collect(Collectors.toList());

            if(kvList.isEmpty()) {
                throw new BootstrapException("no records found");
            } else if(kvList.size() > 1) {
                throw new BootstrapException("multiple records found");
            } else {
                logger.info("file from path {} successfully downloaded", consulPath);
                return kvList.get(0);
            }
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", fileURL);
            throw new BootstrapException("Cannot connect to url " + fileURL, e);
        } finally {
            close(connection);
        }
    }

    public void downloadAndSave(ExternalFileMappings mappings) throws BootstrapException {
        for(ExternalFileMapping mapping : mappings.getMappings()) {
            downloadAndSave(mapping.getPath(), mapping.getFilename());
        }
    }

    private void downloadAndSave(String consulPath, String path) throws BootstrapException {
        ConsulKv kv = executeWithRetry(() -> downloadEntry(consulPath));

        File file = new File(path);

        try {
            logger.info("saving file from consul path {} to path {}", consulPath, path);

            Files.write(file.toPath(), kv.getValue().getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new BootstrapException("Cannot save consul file " + consulPath + " to path " + path, e);
        }
    }

    public static void main(String[] args) throws BootstrapException {

        ApplicationBootstrapBaseConfiguration configuration = ApplicationBootstrapBaseConfiguration.Builder
                .instance()
                .setBaseUrl("http://localhost:8500")
                .isEnabled(true)
                .isRetryWhenFailed(true)
                .setPauseBetweenRetries(5000)
                .setNumberOfRetries(5)
                .build();

        ConsulKvClient kvClient = new ConsulKvClient(configuration);

        /*
        ConsulCheckRequest checkRequest = new ConsulCheckRequest();
        checkRequest.setName("actuator health check");
        checkRequest.setId(UUID.randomUUID().toString());
        checkRequest.setInterval("10s");
        checkRequest.setNotes("none");
        checkRequest.setDeregisterCriticalServiceAfter("10m");
        checkRequest.setHttp("http://192.168.1.73:25110/management/health");
        checkRequest.setMethod("GET");

        ConsulRegisterServiceRequest request = new ConsulRegisterServiceRequest();
        request.setName("authentication-service");
        request.setId(UUID.randomUUID().toString());
        request.setAddress("192.168.1.73");
        request.setPort(25110);
        request.setEnableTagOverride(false);
        request.setCheck(checkRequest);


        kvClient.registerAgentService(request);
        */

        //kvClient.unRegisterService("9ebb509d-7b88-4768-a75e-6dc1b1ff90f0");

        /*
        List<ConsulCatalogNodesForServiceResponse> responses = kvClient.listCatalogNodesForService("authentication-service");
        System.out.println(responses);
        */
    }
}