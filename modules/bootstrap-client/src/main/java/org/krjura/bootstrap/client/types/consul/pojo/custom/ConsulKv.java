package org.krjura.bootstrap.client.types.consul.pojo.custom;

import org.krjura.bootstrap.client.types.consul.pojo.response.kv.ConsulRawKvResponse;

import java.util.Base64;

public class ConsulKv {

    private String key;

    private String value;

    public ConsulKv(ConsulRawKvResponse kv) {
        this.key = kv.getKey();
        this.value = new String(Base64.getDecoder().decode(kv.getValue().getBytes()));
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ConsulKv{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
