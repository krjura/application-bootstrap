package org.krjura.bootstrap.client.types.consul.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.krjura.bootstrap.client.ex.BootstrapException;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.function.Function;

public class ConsulClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulClient.class);

    static final String HTTP_HEADER_CONNECTION = "Connection";
    static final String HTTP_HEADER_CONNECTION_CLOSE = "close";
    static final String HTTP_HEADER_CONSUL_TOKEN = "X-Consul-Token";

    private ApplicationBootstrapBaseConfiguration baseConfiguration;

    private ObjectMapper objectMapper;

    ConsulClient(ApplicationBootstrapBaseConfiguration baseConfiguration) {
        this.baseConfiguration = baseConfiguration;

        createObjectMapper();
    }

    private void createObjectMapper() {
        this.objectMapper = new ObjectMapper();
    }

    ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    ApplicationBootstrapBaseConfiguration getBaseConfiguration() {
        return baseConfiguration;
    }

    void checkResponseCode(HttpURLConnection connection) throws BootstrapException, IOException {
        int responseCode = connection.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_OK) {
            byte[] data = readErrorResponse(connection);
            String response = new String(data);
            logger.warn("Server returned response code {}: response was \n '{}'", responseCode, response);

            throw new BootstrapException("Server returned response code "+ responseCode);
        }
    }

    private byte[] readErrorResponse(HttpURLConnection connection) {
        try {
            InputStream is = connection.getErrorStream();

            if(is == null) {
                return new byte[0];
            }

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        } catch (IOException e) {
            logger.warn("Cannot read input stream", e);

            return new byte[0];
        }
    }

    void close(HttpURLConnection connection) {
        if(connection != null) {
            connection.disconnect();
        }
    }

    <R> R executeWithRetry(VoidFunction<R> function) throws BootstrapException {
        if(!getBaseConfiguration().isRetryWhenFailed()) {
            return function.apply();
        } else if(getBaseConfiguration().getNumberOfRetries() <= 0) {
            return function.apply();
        } else {
            return retry(function);
        }
    }

    <R> R retry(VoidFunction<R> function) throws BootstrapException {
        for (int i = 0; i < getBaseConfiguration().getNumberOfRetries(); i++) {

            try {
                return function.apply();
            } catch ( Exception e) {
                logger.warn(
                        "Cannot execute function. Will try again in " +
                                getBaseConfiguration().getPauseBetweenRetries() + " ms", e);
            }

            try {
                Thread.sleep(getBaseConfiguration().getPauseBetweenRetries());
            } catch (InterruptedException e) {
                logger.info("interrupted");
            }
        }

        throw new BootstrapException(
                "Failed to execute function. Tried " + getBaseConfiguration().getNumberOfRetries() + " times");
    }
}
