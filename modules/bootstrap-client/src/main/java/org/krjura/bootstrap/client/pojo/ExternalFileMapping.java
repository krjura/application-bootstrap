package org.krjura.bootstrap.client.pojo;

public class ExternalFileMapping {

    private String path;

    private String filename;

    public ExternalFileMapping() {
        // mappers
    }

    public ExternalFileMapping(String path, String filename) {
        this.path = path;
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
