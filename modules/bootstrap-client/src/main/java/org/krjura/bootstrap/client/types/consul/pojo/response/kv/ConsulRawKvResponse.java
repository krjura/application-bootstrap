package org.krjura.bootstrap.client.types.consul.pojo.response.kv;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulRawKvResponse {

    private Integer lockIndex;

    private String key;

    private Integer flags;

    private String value;

    private Integer createIndex;

    private Integer modifyIndex;

    @JsonCreator
    public ConsulRawKvResponse(
            @JsonProperty("LockIndex") Integer lockIndex,
            @JsonProperty("Key") String key,
            @JsonProperty("Flags") Integer flags,
            @JsonProperty("Value") String value,
            @JsonProperty("CreateIndex") Integer createIndex,
            @JsonProperty("ModifyIndex") Integer modifyIndex) {

        this.lockIndex = lockIndex;
        this.key = key;
        this.flags = flags;
        this.value = value;
        this.createIndex = createIndex;
        this.modifyIndex = modifyIndex;
    }

    public Integer getLockIndex() {
        return lockIndex;
    }

    public String getKey() {
        return key;
    }

    public Integer getFlags() {
        return flags;
    }

    public String getValue() {
        return value;
    }

    public Integer getCreateIndex() {
        return createIndex;
    }

    public Integer getModifyIndex() {
        return modifyIndex;
    }

    @Override
    public String toString() {
        return "ConsulKv{" +
                "lockIndex=" + lockIndex +
                ", key='" + key + '\'' +
                ", flags=" + flags +
                ", value='" + value + '\'' +
                ", createIndex=" + createIndex +
                ", modifyIndex=" + modifyIndex +
                '}';
    }
}
