package org.krjura.bootstrap.autoconfigure.cloud.config;

import org.krjura.bootstrap.client.ApplicationBootstrap;
import org.krjura.bootstrap.client.internal.pojo.ApplicationBootstrapBaseConfiguration;
import org.krjura.bootstrap.client.pojo.ExternalFileMappings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;

import java.util.HashMap;
import java.util.Map;

public class ApplicationBootstrapPropertySourceLocator implements PropertySourceLocator {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationBootstrapPropertySourceLocator.class);

    private final ApplicationBootstrapConfigConfiguration configConfiguration;

    private final ApplicationBootstrapConfigBaseConfiguration baseConfiguration;

    public ApplicationBootstrapPropertySourceLocator(
            ApplicationBootstrapConfigConfiguration configConfiguration,
            ApplicationBootstrapConfigBaseConfiguration baseConfiguration) {

        this.configConfiguration = configConfiguration;
        this.baseConfiguration = baseConfiguration;
    }

    @Override
    public PropertySource<?> locate(Environment environment) {

        logger.info("Trying to locate bootstrap source");

        applyBootstrapConfiguration();

        Map<String, Object> mapSource = new HashMap<>();
        mapSource.put("org.krjura.bootstrap.config.type", "consul");

        return new MapPropertySource("application-bootstrap-config", mapSource);
    }

    private void applyBootstrapConfiguration() {

        if(!this.baseConfiguration.isEnabled()) {
            return;
        }

        ApplicationBootstrapBaseConfiguration configuration = ApplicationBootstrapBaseConfiguration.Builder
                .instance()
                .setBaseUrl(this.baseConfiguration.getBaseUrl())
                .setToken(this.baseConfiguration.getToken())
                .isEnabled(this.baseConfiguration.isEnabled())
                .isRetryWhenFailed(this.baseConfiguration.isRetryWhenFailed())
                .setNumberOfRetries(this.baseConfiguration.getNumberOfRetries())
                .setPauseBetweenRetries(this.baseConfiguration.getPauseBetweenRetries())
                .build();

        ApplicationBootstrap.bootstrap(configuration, new ExternalFileMappings(this.configConfiguration.getMappings()));
    }
}
