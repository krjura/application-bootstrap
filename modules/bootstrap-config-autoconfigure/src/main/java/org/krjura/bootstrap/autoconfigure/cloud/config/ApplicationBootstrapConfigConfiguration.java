package org.krjura.bootstrap.autoconfigure.cloud.config;

import org.krjura.bootstrap.client.pojo.ExternalFileMapping;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "org.krjura.bootstrap.config")
public class ApplicationBootstrapConfigConfiguration {

    private List<ExternalFileMapping> mappings;

    public ApplicationBootstrapConfigConfiguration() {
        this.mappings = new ArrayList<>();
    }

    public List<ExternalFileMapping> getMappings() {
        return mappings;
    }

    public void setMappings(List<ExternalFileMapping> mappings) {
        this.mappings = mappings;
    }
}
