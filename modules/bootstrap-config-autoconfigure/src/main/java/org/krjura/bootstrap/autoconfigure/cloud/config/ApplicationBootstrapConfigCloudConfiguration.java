package org.krjura.bootstrap.autoconfigure.cloud.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This will be used by spring cloud
 */
@Configuration
@ConditionalOnProperty(value = "org.krjura.bootstrap.config.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({PropertySourceLocator.class})
public class ApplicationBootstrapConfigCloudConfiguration {

    @Bean
    public ApplicationBootstrapConfigConfiguration bootstrapClientConfiguration() {
        return new ApplicationBootstrapConfigConfiguration();
    }

    @Bean
    public ApplicationBootstrapConfigBaseConfiguration applicationBootstrapBaseConfiguration() {
        return new ApplicationBootstrapConfigBaseConfiguration();
    }

    @Bean
    public ApplicationBootstrapPropertySourceLocator bootstrapPropertySourceLocator(
            ApplicationBootstrapConfigConfiguration configConfiguration,
            ApplicationBootstrapConfigBaseConfiguration baseConfiguration) {

        return new ApplicationBootstrapPropertySourceLocator(configConfiguration, baseConfiguration);
    }
}
