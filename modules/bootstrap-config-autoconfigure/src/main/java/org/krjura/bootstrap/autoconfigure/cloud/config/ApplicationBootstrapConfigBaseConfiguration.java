package org.krjura.bootstrap.autoconfigure.cloud.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "org.krjura.bootstrap.base")
public class ApplicationBootstrapConfigBaseConfiguration {

    private String baseUrl = "http://localhost:85000";

    private String token;

    private boolean enabled = true;

    private boolean retryWhenFailed = true;

    private Integer numberOfRetries = 5;

    private Integer pauseBetweenRetries = 5000;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isRetryWhenFailed() {
        return retryWhenFailed;
    }

    public void setRetryWhenFailed(boolean retryWhenFailed) {
        this.retryWhenFailed = retryWhenFailed;
    }

    public Integer getNumberOfRetries() {
        return numberOfRetries;
    }

    public void setNumberOfRetries(Integer numberOfRetries) {
        this.numberOfRetries = numberOfRetries;
    }

    public Integer getPauseBetweenRetries() {
        return pauseBetweenRetries;
    }

    public void setPauseBetweenRetries(Integer pauseBetweenRetries) {
        this.pauseBetweenRetries = pauseBetweenRetries;
    }
}
