#!groovy

node {

  emailRecipients = 'krjura@gmail.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()
    throw err
  }
}

def build() {

  resolveProperties()

  withCredentials([
    string(credentialsId: 'ostoWebUrl', variable: 'REPOSITORY_URL'),
    usernamePassword(credentialsId: 'ostoWebCredentials', passwordVariable: 'REPOSITORY_USERNAME', usernameVariable: 'REPOSITORY_PASSWORD')
  ]) {
    withEnv([
      'SERVER_REPO_HOME=/opt/jenkins/gradle/cache',
      'GRADLE_USER_HOME=/opt/cache/gradle',
      'ENVIRONMENT=jenkins']) {
        docker.image('krjura/build-env:4').inside("-v ${SERVER_REPO_HOME}:${GRADLE_USER_HOME}") {
          stage('Initialize') {
            checkout scm
          }

          stage('Build, Test, Deploy') {
            // execute modules/bootstrap-client
            sh './build-utils --module modules/bootstrap-client --branch ${BRANCH_NAME} --env $ENVIRONMENT --op publishProject'
            sh './build-utils --module modules/bootstrap-config-autoconfigure --branch ${BRANCH_NAME} --env $ENVIRONMENT --op publishProject'
            sh './build-utils --module modules/bootstrap-discovery-autoconfigure --branch ${BRANCH_NAME} --env $ENVIRONMENT --op publishProject'
          }
        }
      }
    }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
    body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
      subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
      body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []

  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '20')))

    config.add(disableConcurrentBuilds())

    properties(config)
}
